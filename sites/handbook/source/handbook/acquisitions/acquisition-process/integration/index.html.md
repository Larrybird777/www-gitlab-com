---
layout: handbook-page-toc
title: "Acquisition Integration"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This is a detailed view of our acquisition integration process. For more information about
our acquisitions approach visit our [acquisitions process](/handbook/acquisitions/acquisition-process/).

## Overview

The DRI for the integration stage is the Dir. Corporate Development. This will consist of several aspects and [best practices](https://www.mckinsey.com/business-functions/strategy-and-corporate-finance/our-insights/how-the-best-acquirers-excel-at-integration):
1. Operational shutdown (if appropriate):
   1. Execution of customer sunset plans
   1. Shutdown of target company and its entities
1. Alignment on objectives and goals:
   1. Incorporate the integration plan into GitLab's roadmap
   1. Introduce Deal Milestones into respective departmental OKRs
   1. Create or update product vision in case a new product area is added to GitLab
1. People & Culture:
   1. Cultural integration plan, analyzing similarities and differences in the two companies' cultures
   1. Onboarding of acquired team members
1. Performance tracking:
   1. Identify and continuously revisit business performance metrics to track the deal's added value and ROI over time
   1. Delivery against product plans and OKRs


### Technical integration

1. Each delivery milestone which is defined in the acquisition agreement will be converted to confidential issue by the PM leading the integration efforts, once the acquisition announcement has been made public. These issues will be the SSOT to track progress on a delivery milestone. Once an issue is completed, it will serve as a confirmation of a successful delivery of that milestone. Signoff for completion of a milestones is required by:
   1. Product champion
   1. Engineering champion
   1. Dir. Corp. Dev.
1. Once a milestone has been signed-off, the Dir. Corp. Dev. will notify the accounting team if there is a consideration payment which needs to be processed
1. Monthly check-in calls will be scheduled by the PM leading the integration efforts with the following team members:
   1. Product and engineering champions
   1. Acquisition lead (Dir. Corp. Dev.)
   1. Main engineer leading the integration efforts from the acquired company


### People integration

1. 2 months after the new team members have joined GitLab, the [acquisition employee survey](https://docs.google.com/document/d/19unq7wVuZLiMklvvTxfRkV3NhI6gm_B6u5ipGUhxP9E/edit) should be sent by the People Ops Business Partner through CultureAmp

### Team enablement

1. **Snapshot video** - Ahead of the announcement the Product champion will record a short video (up to 10 minutes) which will be included as part of the PR for the acquisition as well as resource for enablement for the GitLab team. The video should cover the following points:
   1. An overview of the categories the acquisition is relevant for
   1. Why these categories are important to GitLab's users and how they fit 
   1. What value will the completed integration of the acquisition deliver to our users
   1. The impact the integration can have on our users
1. **Prospect FAQ** - A prospect FAQ document will be created answering questions which are likely to come up from our customers
1. **Overview webcast** - A webcast for our field teams will be scheduled 1-2 months from announcement providing a more in-depth overview of the acquisition and its impact for our product, customers and prospects.
1. **Technical webcast** - Scheduled around the delivery of the MVC of the integration efforts, a technical webcast for our field teams will be scheduled to provide a demo of the new functionality and new product workflows.
