---
layout: handbook-page-toc
title: "Demystifying the Metrics Conversation"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Many sales reps find the Metrics portion of the Command of the Message customer conversation to be a difficult one for the following reasons:
- Many customers don’t have good metrics
- Customers may be reluctant to share metric details for negotiation purposes (they don't want to admit how compelling of an ROI GitLab can provide!)
- Challenge with finding the perfect proof point(s) to demonstrate how we've helped other organizations
- Difficulty connecting Metrics with Positive Business Outcomes (PBOs)
- Metrics, if used in the wrong way, can get complex and technical

The following sections outline practical tips to help demystify the Metrics conversation and overcome the challenges identified above. 

## Embrace Your Role as the Trusted Advisor


## Demonstrate Empathy


## Be a Good Storyteller


