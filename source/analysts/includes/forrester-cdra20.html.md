---
layout: markdown_page
title: "The Forrester Wave: Continuous Delivery And Release Automation, Q2 2020"
---
## GitLab and the Forrester Wave for Continuous Delivery and Release Automation (CDRA) 2020*

This page represents how Forrester views our CDRA  capabilities in relation to the larger market and how we're working with that information to build a better product. It is also intended to provide Forrester with ongoing context for how our product is developing in relation to how they see the market.

A link to the report can be found [here](https://www.forrester.com/report/The+Forrester+Wave+Continuous+Delivery+And+Release+Automation+Q2+2020/-/E-RES157265). 

### Forrester's Key Takeaways on the CDRA Market at time of report publication:

**CloudBees, IBM, Microsoft, Digital.ai, Broadcom, And Flexagon Lead The Pack**
Forrester's research uncovered a market in which CloudBees, IBM, Microsoft, Digital.ai, Broadcom, and Flexagon are Leaders; GitLab, Micro Focus, Chef, and Harness are Strong Performers; and Inedo, Armory, Atlassian, and Octopus Deploy are Contenders.

**Visualizations Of Complex Application And Deployment Models Are Key Differentiators**
As the continuous delivery market continues to consolidate, and with upstream continuous integration capabilities and higher-order management becoming the norm, vendors are competing increasingly on breadth of functionality. The ability to visualize complex application and deployment models continues to be a differentiator, as does the management of deployment outcomes. Appropriate use of advanced analytics and machine learning is also a key factor, with continuing vendor investment resulting in valuable capabilities such as improved release readiness.

### Forrester's take on GitLab at time of report publication:

**GitLab is expanding its comprehensive platform quickly.** GitLab emerged from the continuous integration side of the market and, with its foundation in source control, has strong headwaters capabilities. GitLab supports continuous integration and deployment to cloud-native platforms, but support for legacy platforms is lacking. More recently, the company has added continuous delivery features, including continuous integration and deployment for Kubernetes. The product bases its application modeling on Helm charts, thus requiring Kubernetes to function. The firm grounds its strategy in a very active open source community and a clear ability to execute on this business mode. GitLab distinguishes itself as one of the fastest-innovating vendors in this evaluation.

GitLab received high marks from reference customers for providing a unified environment for product owners and developers as well as for its speed and product vision. Reference customers suggested improvements to areas like work planning and noted that the difference in price between silver and gold is exorbitant for what the solution provides. GitLab's platform support, release modeling, and deployment execution are lacking compared with those of other platforms. GitLab's release automation is ideal for cloud-native, Kubernetes-centric organizations. It still has a long way to go to fully support the broader legacy world.

## GitLab's commentary on this report

### Lessons Learned

